Rails.application.routes.draw do
  root 'default#user-login'
  get 'blank' => 'theme#blank'
  get 'cards-audio' => 'theme#cards-audio'
  get 'cards-video' => 'theme#cards-video'
  get 'cards-image' => 'theme#cards-image'
  get 'charts-c3' => 'theme#charts-c3'
  get 'charts-chartjs' => 'theme#charts-chartjs'
  get 'charts-flot' => 'theme#charts-flot'
  get 'charts-knob' => 'theme#charts-knob'
  get 'components-offline-detector' => 'theme#components-offline-detector'
  get 'components-session-timeout' => 'theme#components-session-timeout'
  get 'components-sound-library' => 'theme#components-sound-library'
  get 'components-tree-view' => 'theme#components-tree-view'
  get 'forms-custom-file-input' => 'theme#forms-custom-file-input'
  get 'forms-dropzone' => 'theme#forms-dropzone'
  get 'forms-general' => 'theme#forms-general'
  get 'forms-icheck' => 'theme#forms-icheck'
  get 'forms-image-cropper' => 'theme#forms-image-cropper'
  get 'forms-jcrop' => 'theme#forms-jcrop'
  get 'forms-layouts' => 'theme#forms-layouts'
  get 'forms-multiple-file-upload' => 'theme#forms-multiple-file-upload'
  get 'forms-pickers' => 'theme#forms-pickers'
  get 'forms-range-sliders' => 'theme#forms-range-sliders'
  get 'forms-selects' => 'theme#forms-selects'
  get 'forms-switchers' => 'theme#forms-switchers'
  get 'forms-tools' => 'theme#forms-tools'
  get 'forms-validation' => 'theme#forms-validation'
  get 'forms-wizard' => 'theme#forms-wizard'
  get 'forms-wysiwyg-editors' => 'theme#forms-wysiwyg-editors'
  get 'grids-bootstrap' => 'theme#grids-bootstrap'
  get 'grids-bootstrap-extra' => 'theme#grids-bootstrap-extra'
  get 'grids-masonry' => 'theme#grids-masonry'
  get 'index-2' => 'theme#index-2'
  get 'maps-google' => 'theme#maps-google'
  get 'maps-leaflet' => 'theme#maps-leaflet'
  get 'maps-vector' => 'theme#maps-vector'
  get 'pages-404' => 'theme#pages-404'
  get 'pages-about-us' => 'theme#pages-about-us'
  get 'pages-blog' => 'theme#pages-blog'
  get 'pages-coming-soon' => 'theme#pages-coming-soon'
  get 'pages-contact-us' => 'theme#pages-contact-us'
  get 'pages-invoice' => 'theme#pages-invoice'
  get 'pages-pricing' => 'theme#pages-pricing'
  get 'pages-search-results' => 'theme#pages-search-results'
  get 'pages-timeline' => 'theme#pages-timeline'
  get 'panels' => 'theme#panels'
  get 'panels-ajax' => 'theme#panels-ajax'
  get 'panels-draggable' => 'theme#panels-draggable'
  get 'panels-styling' => 'theme#panels-styling'
  get 'table-bootstrap-table' => 'theme#table-bootstrap-table'
  get 'table-datatables' => 'theme#table-datatables'
  get 'table-datatables-data-sources' => 'theme#table-datatables-data-sources'
  get 'table-datatables-editor' => 'theme#table-datatables-editor'
  get 'table-datatables-extensions' => 'theme#table-datatables-extensions'
  get 'table-elements' => 'theme#table-elements'
  get 'table-responsive' => 'theme#table-responsive'
  get 'ui-elements-buttons' => 'theme#ui-elements-buttons'
  get 'ui-elements-dialogs' => 'theme#ui-elements-dialogs'
  get 'ui-elements-font-icons' => 'theme#ui-elements-font-icons'
  get 'ui-elements-general' => 'theme#ui-elements-general'
  get 'ui-elements-lists' => 'theme#ui-elements-lists'
  get 'ui-elements-notifications' => 'theme#ui-elements-notifications'
  get 'ui-elements-social-buttons' => 'theme#ui-elements-social-buttons'
  get 'ui-elements-tabs' => 'theme#ui-elements-tabs'
  get 'ui-elements-typography' => 'theme#ui-elements-typography'
  get 'user-calendar' => 'theme#user-calendar'
  get 'user-lock-screen' => 'theme#user-lock-screen'
  get 'user-login' => 'theme#user-login'
  get 'user-profile' => 'theme#user-profile'

  get 'bankaccount' => 'default#bankaccount'
  get 'penalty' => 'default#penalty'
  get  'nonregular' => 'default#nonregular'
  get 'invoice' => 'default#invoice'

  
  get 'Event_generate' => 'default#Event_generate'

  get 'discount_charge' => 'default#discount_charge'

 




  get 'document-repository' => 'default#document-repository'
  get 'mem-contact-list' => 'default#mem-contact-list'
  get 'tenant-reg' => 'default#tenant-reg'

  get 'ownerreg' => 'default#ownerreg'
  get 'complain' => 'default#complain'
  get 'Committemember-list' => 'default#Committemember-list'
  get 'emailbroadcast' => 'default#emailbroadcast'
  get 'upcomming_meeting' => 'default#upcomming_meeting'
  get 'Previous_meetings' => 'default#Previous_meetings'

  get 'member_search' => 'default#member_search'
  get 'member_achievement' => 'default#member_achievement'


  get 'maintenance_charge' => 'default#maintenance_charge'

  get 'My_Flat' => 'default#My_Flat'
  get 'Notice_Board' => 'default#Notice_Board'
  get 'Polling' => 'default#Polling'
  get 'Event' => 'default#Event'
 


  get 'comite_memberreg' => 'default#comite_memberreg'
  get 'Noticeboard' => 'default#Noticeboard'
  get 'attend_track' => 'default#attend_track'

  get 'Vender_form' => 'default#Vender_form'
  


  get 'expence_bill' => 'default#expence_bill'
  get 'meetting_add' => 'default#meetting_add'
  get 'test' => 'default#test'
  get 'vender_detail' => 'default#vender_detail'
  get 'polling_record' => 'default#polling_record'
  get 'FAQ' => 'default#FAQ'
  get 'E-filling of TDS' => 'default#E-filling of TDS'
  get 'it return' => 'default#it return'
  





  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
